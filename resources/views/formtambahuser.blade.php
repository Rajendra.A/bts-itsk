<!-- Form untuk menambah user -->
<form action="{{ route('usersmanagement.store') }}" method="POST">
    @csrf
    <input type="text" name="nama" placeholder="Nama">
    <input type="text" name="nim" placeholder="NIM">
    <input type="email" name="email" placeholder="Email">
    <input type="password" name="password" placeholder="Password">
    <select name="role">
        <option value="Dosen">Dosen</option>
        <option value="Mahasiswa">Mahasiswa</option>
        <option value="Admin">Admin</option>
    </select>
    <input type="text" name="prodi" placeholder="Prodi">
    <button type="submit">Tambah User</button>
</form>
