@extends('admin.dashboard.layouts.main')

@php
    $title = 'File Pendukung';
@endphp

@section('title')
    Dashboard Tambah File Pendukung
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('file_pendukung.store') }}" method="POST" enctype="multipart/form-data">
            <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Tambah File Pendukung</h1>

            @csrf
            <div class="mb-4">
                <label for="nama" class="form-label fw-semibold">Nama File</label>
                <input type="text" class="form-control p-2" id="nama" name="nama"
                    placeholder="Masukkan Nama File" required>
            </div>

            <div class="mb-4">
                <label for="jenis" class="form-label fw-semibold">Jenis File</label>
                <select class="form-control" name="jenis" id="jenis" required>
                    <option value="" selected disabled hidden>Pilih Jenis File</option>
                    <option value="word">WORD</option>
                    <option value="ppt">PPT</option>
                    <option value="pdf">PDF</option>
                </select>
            </div>

            <div class="mb-4">
                <label for="file" class="form-label fw-semibold">Upload File</label>
                <input type="file" class="form-control p-2" id="file" name="file"
                    accept=".pdf,.ppt,.pptx,.doc,.docx" required>
            </div>

            <button type="submit" class="btn btn-dark d-block mx-auto mt-5 px-5 py-2">Simpan</button>
        </form>
    </div>
@endsection