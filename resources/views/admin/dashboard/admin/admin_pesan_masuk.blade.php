@extends('admin.dashboard.layouts.main')

@php
    $title = 'Admin';
@endphp

@section('title')
    Dashboard Pesan Masuk
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <style>
        .pesan {
            background-color: rgb(250, 250, 250);
        }

        .pesan:hover {
            background-color: rgb(240, 240, 240);
        }
    </style>

    <h1 class="fs-4 fw-bold mx-3 mb-3" style="margin-top: 100px">Pesan Masuk</h1>
    <div class="col-12 border-top">
        {{-- Pesan --}}
        @forelse($questions as $question)
        <a href="{{ route('balas.pesan', $question->id) }}"
        class="pesan d-flex text-dark text-decoration-none px-2 py-3 border-bottom">
            <div class="col-2 col-sm-1">
                <img src="{{ asset('img/profile.png') }}" alt="Foto Profil" class="d-block mx-auto" style="width: 45px">
            </div>
            <div class="col-8 col-sm-9 px-2">
                <h1 class="fs-6">{{ $question->user->nama }}</h1>
                <p class="m-0">{{ $question->question_content }}</p>
            </div>
            <div class="col-2 d-flex flex-column justify-content-start align-items-center">
                <small class="text-center">{{ $question->created_at->format('d F Y') }}</small>
                <span class="text-light"
                    style="background-color: #19D242; padding: 0 8px; margin-top: 7px; border-radius: 50%">
                    1
                </span>
            </div>
        </a>
        @empty
            <div class="pesan d-flex text-dark text-decoration-none px-2 py-3 border-bottom">
                <div class="col-12 text-center">
                    <p class="m-0">Tidak ada pesan masuk.</p>
                </div>
            </div>
        @endforelse
    </div>
@endsection