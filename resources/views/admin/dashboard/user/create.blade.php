@extends('admin.dashboard.layouts.main')

@php
    $title = 'User';
@endphp

@section('title')
    Dashboard Tambah User
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
        <form action="{{ route('usersmanagement.store') }}" method="POST">
            @csrf
            <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Tambah Data User</h1>

            <div class="mb-4">
                <label for="nama" class="fw-semibold mb-2">Nama</label>
                <input type="text" class="form-control p-2" id="nama" name="nama" placeholder="Masukkan Nama"
                    required>
            </div>

            <div class="mb-4">
                <label for="nim" class="fw-semibold mb-2">NIM</label>
                <input type="text" class="form-control p-2" id="nim" name="nim" placeholder="Masukkan NIM"
                    required>
            </div>

            <div class="mb-4">
                <label for="email" class="fw-semibold mb-2">Email</label>
                <input type="email" class="form-control p-2" id="email" name="email" placeholder="Masukkan Email"
                    required>
            </div>

            <div class="mb-4">
                <label for="password" class="fw-semibold mb-2">Password</label>
                <input type="password" class="form-control p-2" id="password" name="password"
                    placeholder="Masukkan Password" required>
            </div>

            <div class="mb-4">
                <label for="role" class="fw-semibold mb-2">Role</label>
                <select id="role" class="form-select p-2" name="role" required>
                    <option value="" selected disabled hidden>Pilih Role</option>
                    <option value="Mahasiswa">Mahasiswa</option>
                    <option value="Dosen">Dosen</option>
                    <option value="Admin">Admin</option>
                </select>
            </div>

            <div class="mb-0">
                <label for="prodi" class="fw-semibold mb-2">Program Studi</label>
                <input type="text" class="form-control p-2" id="prodi" name="prodi"
                    placeholder="Masukkan Program Studi" required>
            </div>

            <button type="submit" class="btn btn-dark d-block mx-auto mt-5 px-5 py-2">Simpan</button>
        </form>
    </div>
@endsection